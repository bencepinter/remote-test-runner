﻿namespace RemoteRunner.Common
{
    public enum RemoteTaskStatus
    {
        TaskReady,
        NoMoreTasks,
        AskAgainLater
    }
}
