﻿using System.Runtime.Serialization;

namespace RemoteRunner.Common
{
    [DataContract]
    public class RemoteTaskInfo
    {
        [DataMember]
        public string TaskId { get; set; }

        [DataMember]
        public string TaskData { get; set; }

        [DataMember]
        public RemoteTaskStatus RemoteTaskStatus { get; set; }
    }
}
