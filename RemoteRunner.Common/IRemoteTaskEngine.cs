﻿using System.ServiceModel;

namespace RemoteRunner.Common
{
    [ServiceContract]
    public interface IRemoteTaskEngine
    {
        [OperationContract]
        RemoteTaskInfo StartNewTask();

        [OperationContract]
        void CompleteTask(string taskId, string taskResult);
    }
}
