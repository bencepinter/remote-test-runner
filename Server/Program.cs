﻿using RemoteRunner.Server;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            var agentCollection = ConfigurationManager.GetSection("agents") as NameValueCollection;
            List<Uri> agents = new List<Uri>();
            foreach (var key in agentCollection.AllKeys)
                agents.Add(new Uri(agentCollection[key]));
            Uri baseAddress = new Uri(ConfigurationManager.AppSettings["baseAddress"]);

            List<RemoteTask> tasks = new List<RemoteTask>();
            tasks.Add(new RemoteTask("Amazon"));
            tasks.Add(new RemoteTask("Google"));
            tasks.Add(new RemoteTask("EPAM"));
            tasks.Add(new RemoteTask("LogMeIn"));
            tasks.Add(new RemoteTask("Microsoft"));

            RemoteTaskRunner runner = new RemoteTaskRunner();
            var results = runner.Run(tasks, baseAddress, agents, 30);

            foreach (RemoteTaskResult result in results)
                Console.WriteLine("{0} : {1}", result.Task.TaskId, result.Completed);

            Console.WriteLine("Done");
            Console.ReadKey();
        }
    }
}
