﻿using System;
using System.Threading;
namespace RemoteRunner.Server
{
    internal class RemoteTaskWrapper
    {
        private int status;
        public int Status { get { return status; } }

        public string Result { get; private set; }
        public RemoteTask Task { get; private set; }

        private int resetCount;
        private readonly int timeoutSeconds;
        private DateTime? startTime;
        public bool IsTimeout
        {
            get
            {
                DateTime? startTimeTemp = startTime;
                if (status != RemoteTaskWrapperStatus.Running || !startTimeTemp.HasValue)
                    return false;

                return (DateTime.UtcNow - startTimeTemp.Value).TotalSeconds > timeoutSeconds;
            }
        }

        public RemoteTaskWrapper(RemoteTask task, int timeoutSeconds = 300)
        {
            status = RemoteTaskWrapperStatus.NotStarted;
            Task = task;
            this.timeoutSeconds = timeoutSeconds;
        }

        public bool BeginExecution()
        {
            int oldStatus = Interlocked.CompareExchange(ref status, RemoteTaskWrapperStatus.Running, RemoteTaskWrapperStatus.NotStarted);
            if (oldStatus != RemoteTaskWrapperStatus.NotStarted)
                return false;

            startTime = DateTime.UtcNow;
            return true;
        }

        public void Reset()
        {
            int oldValue = Interlocked.Increment(ref resetCount);

            if (oldValue > 3)
                Interlocked.CompareExchange(ref status, RemoteTaskWrapperStatus.Failed, RemoteTaskWrapperStatus.Running);
            else
                Interlocked.CompareExchange(ref status, RemoteTaskWrapperStatus.NotStarted, RemoteTaskWrapperStatus.Running);
        }

        public void CompleteExecution(string result)
        {
            int oldStatus = Interlocked.CompareExchange(ref status, RemoteTaskWrapperStatus.Completed, RemoteTaskWrapperStatus.Running);
            if (oldStatus != RemoteTaskWrapperStatus.Running)
                return;

            Result = result;
        }
    }
}
