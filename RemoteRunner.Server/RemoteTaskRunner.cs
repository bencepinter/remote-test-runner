﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace RemoteRunner.Server
{
    public sealed class RemoteTaskRunner
    {
        public IEnumerable<RemoteTaskResult> Run(IEnumerable<RemoteTask> tasks, Uri baseAddress, IEnumerable<Uri> agents, int taskTimeoutSeconds = 300)
        {
            RemoteTaskEngine taskEngine = new RemoteTaskEngine(baseAddress, agents);

            taskEngine.Run(tasks.Select(task => new RemoteTaskWrapper(task, taskTimeoutSeconds)));

            return taskEngine.Tasks.Select(task => new RemoteTaskResult(task.Task, task.Result, task.Status == RemoteTaskWrapperStatus.Completed)).ToList();
        }
    }
}
