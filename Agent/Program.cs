﻿using System;
using System.Configuration;

namespace Agent
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomAgent agent = new CustomAgent();

            Uri baseAddress = new Uri(ConfigurationManager.AppSettings["baseAddress"]);

            agent.Open(baseAddress);

            Console.WriteLine("Agent running, press <Enter> to terminate");
            Console.ReadKey();

            agent.Close();
        }
    }
}
